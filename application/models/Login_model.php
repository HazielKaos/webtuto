<?php
/**
 * Created by PhpStorm.
 * User: romuald
 * Date: 02-08-19
 * Time: 21:49
 */

class Login_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function register(){
        //TOUT CE QUI EST ICI VA S'OCCUPER D'AJOUTER L'UTILISATEUR A LA BASE DE DONEE (INSCRIPTION)
        $pseudo = $this->input->post('pseudo', true);
        $password = $this->input->post('password');
        $email = $this->input->post('email', true);


        $check1 = $this->db->get_where("user", array("pseudo" => $pseudo));
        if ($check1->num_rows() != 0){
            $this->session->set_flashdata('error', "Un utilisateur a déja ce pseudo");
            redirect("/");
        }else{
          $check2 = $this->db->get_where("user", array('email' => $email));
          if ($check2->num_rows() != 0){
              $this->session->set_flashdata('error', "Un utilisateur a déja cette adresse email");
              redirect("/");
          }else {
              // Pas de corespondance pseudo et email dans a base de donnée
              $passCrypt = $this->crypt($password);
              $date = time();
              $user = array(
                  'pseudo' => $pseudo,
                  'password' => $passCrypt,
                  'email' => $email,
                  'first_date' => $date,
                  'last_date' => $date,
                  'rank' => 1
              );

              if ($this->db->insert('user', $user)){
                  //OK
                  $this->session->set_userdata('pseudo', $pseudo);
                  $this->session->set_flashdata("success", "Vous êtes bien inscrit sur le site");
                  redirect("/");
              }else{
                  //ERREUR BASE DE DONNEE
                  $this->session->set_flashdata('error', "Erreur de SQL ");
                  redirect("/");
              }
          }
        }
    }

    public function login(){
        $pseudo = $this->input->post('pseudo', true);
        $password = $this->input->post('password');
        $check1 = $this->db->get_where('user', array('pseudo' => $pseudo));
        if ($check1->num_rows() != 0){
            $user = $check1->row();
            if ($this->checkCrypt($password, $user->password)){
                $date = time();
                $this->db->where('pseudo', $pseudo);
                //$this->db->set('last_date', $date);
                if ($this->db->update("user", array('last_date' => $date))){
                    $this->session->set_userdata('pseudo', $pseudo);
                    $this->session->set_flashdata("success", "Vous êtes bien connecté sur le site");
                    redirect("/");
                }else{
                    $this->session->set_flashdata('error', "Erreur SQL ");
                    redirect("/");
                }
            }else{
                $this->session->set_flashdata('error', "Le mot de passe semble incorrect");
                redirect("/");
            }
        }else {
            $this->session->set_flashdata('error', "Le compte utilisateur est introuvable");
            redirect("/");
        }
    }

    public function logout(){
        $this->session->unset_userdata("pseudo");
        redirect("/");
    }

    private function crypt(String $pass): string
    {
        return password_hash($pass, PASSWORD_DEFAULT);
    }

    private function checkCrypt(string $pass, string $crypt) : bool
    {
        return password_verify($pass, $crypt);
    }


}