<?php
/**
 * Created by PhpStorm.
 * User: romuald
 * Date: 02-08-19
 * Time: 22:43
 */

class User_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getUserData(string $pseudo) :stdClass {
        $sql = $this->db->get_where('user', array("pseudo" => $pseudo));
        if ($sql->num_rows() == 0){
            return null;
        }else{
            return $sql->row();
        }
    }

    public function getRank(int $rankID) : stdClass {
        $sql = $this->db->get_where('rank', array("id" => $rankID));
        if ($sql->num_rows() == 0){
            return null;
        }else{
            return $sql->row();
        }
    }
}