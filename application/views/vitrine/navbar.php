<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="/">Mon Super Site</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <?php
                if ($this->session->userdata('pseudo')) {


                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/">Espace membre</a>
                    </li>
                    <?php
                    if ($rankInfo->admin == 1){ ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/">Espace admin</a>
                        </li>
                    <?php }
                    ?>


                    <?php
                }
                    ?>
            </ul>

            <?php
            if (!$this->session->userdata('pseudo')){
            ?>
           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Login
            </button>
            <?php }

            else { ?>
                <a href="logout" class="btn btn-primary" >
                    <?php echo $rankInfo->name. "  -  ".  $this->session->userdata("pseudo");?>
                </a>
            <?php }
            ?>

        </div>
    </nav>
</header>