<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model('user_model');

    }

    public function index()
    {
        $this->load->view('vitrine/head');
        if ($this->session->userdata('pseudo')) {
            $userInfo = $this->user_model->getUserData($this->session->userdata('pseudo'));
            $rankInfo = $this->user_model->getRank($userInfo->rank);
            $data['userInfo'] = $userInfo;
            $data['rankInfo'] = $rankInfo;
            $this->load->view('vitrine/navbar', $data);
        }
        $this->load->view('vitrine/navbar');
        $this->load->view('vitrine/main');
        $this->load->view('vitrine/modal');
        $this->load->view('vitrine/foot');
    }
}
