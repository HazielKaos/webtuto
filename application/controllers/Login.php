<?php
/**
 * Created by PhpStorm.
 * User: romuald
 * Date: 02-08-19
 * Time: 21:14
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('login_model');

    }


    public function register()
    {
        //VERIFICATION DES INPUTS
        $this->form_validation->set_rules('pseudo', 'Pseudo', 'required');
        $this->form_validation->set_rules('email', 'Adresse email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Mot de passe', 'required');
        $this->form_validation->set_rules('password2', 'Password Confirmation', 'required');
        if ($this->form_validation->run() === FALSE) {
            //AFFICHE UN MESSAGE D'ERREUR
            $this->session->set_flashdata('error', "Des champs sont manquants dans le formulaire d'inscription");
            redirect("/");
        } else {
            $this->form_validation->set_rules('password2', 'Password Confirmation', 'matches[password]');
            if ($this->form_validation->run() === FALSE) {
                $this->session->set_flashdata('error', "Le mot de passe et sa confirmation ne correspondent pas");
                redirect("/");
            } else {
                $this->login_model->register();
            }
        }


    }

    public function login()
    {
        $this->form_validation->set_rules('pseudo', "Pseudo", "Required");
        $this->form_validation->set_rules('password', "Mot de passe", "Required");
        if ($this->form_validation->run() === FALSE){
            $this->session->set_flashdata('error', "Des champs sont manquants dans le formulaire de connexion");
            redirect("/");
        }else{
            $this->login_model->login();
        }
    }

    public function logout()
    {
        if (!$this->session->userdata("pseudo")){
            redirect("/");
        }else{
            $this->login_model->logout();
        }
    }
}